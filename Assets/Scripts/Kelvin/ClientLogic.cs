﻿using UnityEngine;
using System.Collections;

public class ClientLogic : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	[RPC]
	public void MatchEnd_ParachuteLaunchCallback( int winnerPlayerID )
	{
		if( uLink.Network.player.id == winnerPlayerID )
		{
			//YOU WIN!!
			Debug.Log("(CLIENT) -- You WIN!!!");
		}
		else
		{
			//YOU LOSE...
			Debug.Log("(CLIENT) -- You LOSE!!!");
        }
	}
}
