﻿using UnityEngine;
using System.Collections;

public class TestCubeMovement : MonoBehaviour 
{
	public float movementSpeed = 1.0f;
	public int moveDistance = 5;
	public int direction = 1;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		float newX = transform.position.x + (movementSpeed * Time.deltaTime) * direction;
		transform.position = new Vector3( newX, uLink.Network.player.id,  0 );
		Debug.Log( "Transform Position: " + transform.position );

		if( transform.position.x > moveDistance )
		{
			transform.position = new Vector3( 
			                                 moveDistance, 
			                                 transform.position.y,
			                                 transform.position.z );
			direction *= -1;

		}

		if( transform.position.x < -moveDistance )
		{
			transform.position = new Vector3( 
			                                 -moveDistance, 
			                                 transform.position.y,
			                                 transform.position.z );
			direction *= -1;
		}
	}
}
