﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ServerLogic : uLink.MonoBehaviour 
{
	public int numPlayersConnected = 0;
	public int MAX_PLAYERS = 1;

	public GameObject parachuteOwner;
	public GameObject parachuteProxy;
	public GameObject parachuteServer;
	
	/// <summary>
	/// The player ID (uLink player ID) of the current
	/// owner of the parachute. If no owner is present,
	/// the value is -1.
	/// </summary>
	public int parachuteOwnerPlayerID = -1;

	public float currentParachuteEquipTimer = 0;
	private const float MAX_EQUIP_TIME = 10.0f; //seconds
	
	void Update () 
	{
		if( parachuteOwnerPlayerID != -1 )
		{
			currentParachuteEquipTimer += Time.deltaTime;

			if( currentParachuteEquipTimer >= MAX_EQUIP_TIME )
			{
				//Player Wins!!
				Debug.Log("Player " + parachuteOwnerPlayerID + " WINS!!!");

				// Notify the logic object of all clients that there is a winner.
				networkView.RPC("MatchEnd_ParachuteLaunchCallback", uLink.RPCMode.Others, parachuteOwnerPlayerID);

				// Remove the assigned value as to not repeat the loop.
				parachuteOwnerPlayerID = -1;
			}
		}
		else
		{
			currentParachuteEquipTimer = 0;
		}
	}

	void uLink_OnPlayerConnected(uLink.NetworkPlayer player)
	{
		++numPlayersConnected;

		Debug.Log("New Player! (" + numPlayersConnected + "/" + MAX_PLAYERS + ")");

		// If all players are in, begin match.
		if( numPlayersConnected >= MAX_PLAYERS )
		{
			Debug.Log("All players connected!!!");

			// Instantiate parachute.
			uLink.Network.Instantiate(
				uLink.NetworkPlayer.server,
			    parachuteProxy, 
				parachuteOwner, 
				parachuteServer,
				new Vector3(0,GameplayProperties.StartingHeight,0),
				Quaternion.identity,
				0 );

			// Activate descent on all players.

		}
	}		
}
