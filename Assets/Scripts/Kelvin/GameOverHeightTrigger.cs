﻿using UnityEngine;
using System.Collections;

public class GameOverHeightTrigger : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other) 
	{
		//If a player hit the trigger
		if( other.tag == "Player" )
		{
			// Enter Game Over Camera
			Debug.Log( "Game Over" );

			// Destroy the trigger locally to avoid repeat collision.
			Destroy ( gameObject );
		}
	}
}
