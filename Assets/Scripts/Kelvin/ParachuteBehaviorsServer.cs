﻿using UnityEngine;
using System.Collections;

public class ParachuteBehaviorsServer : FallingObject 
{

	// Use this for initialization
	public override void Start () 
	{
		base.Start();
	}
	
	// Update is called once per frame
	public override void Update () 
	{
		// Run the base class update, which performs the falling 
		base.Update();
    }

	/// <summary>
	/// Network callback for when the parachute is acquired.
	/// </summary>
	/// <param name="parachuteCollidingPlayerID">uLink ID for the player that caught the parachute.</param>
	[RPC]
	void ParachuteAcquiredCallback( int parachuteCollidingPlayerID )
	{
		Debug.Log( "(SERVER) -- Player " + parachuteCollidingPlayerID + " caught the parachute.");

		//Assign parachute owner via uLink player ID
		GameObject serverLogicObject = GameObject.Find("ServerLogic");
		ServerLogic serverLogicScript = serverLogicObject.GetComponent<ServerLogic>();
		serverLogicScript.parachuteOwnerPlayerID = parachuteCollidingPlayerID;

		//Destroy the parachute
		uLink.Network.Destroy( networkView );
	}
}
