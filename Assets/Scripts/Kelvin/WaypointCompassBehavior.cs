﻿using UnityEngine;
using System.Collections;

public class WaypointCompassBehavior : MonoBehaviour 
{
	//Needs to be set when the parachute becomes available
	public Transform parachuteTransform;

	// Update is called once per frame
	void Update () 
	{
		if( null != parachuteTransform )
		{
			Vector3 parachuteToPlayer = parachuteTransform.position - transform.position;
			transform.rotation = Quaternion.LookRotation(parachuteToPlayer.normalized);
		}
		else
		{
			GameObject parachute = GameObject.FindGameObjectWithTag("Parachute");
			if( null != parachute )
			{
				parachuteTransform = parachute.transform;
			}
		}
	}
}
