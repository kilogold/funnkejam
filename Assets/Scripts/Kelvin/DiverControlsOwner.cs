﻿using UnityEngine;
using System.Collections;

public class DiverControlsOwner : FallingObject 
{
	public float rotationSpeed = 5.0f;
	public float panningSpeed = 20.0f;
	private Transform fwdViewTransform;

	// Use this for initialization
	public override void Start () 
	{
		//Set initial position
		transform.position = new Vector3( 
		                                 Random.Range(0,40),
		                                 transform.position.y,
		                                 Random.Range(0,40)
		                                 );

		//Acquire forward view
		fwdViewTransform = GameObject.Find("OVRCameraController").transform;
		base.Start();
	}
	
	// Update is called once per frame
	public override void Update () 
	{
		// Dive mechanism
        float controlsDive = Input.GetAxis("Vertical");

		if( controlsDive < 0 )
		{
			dragReductionScalar = 0.5f;
		}
        else if( controlsDive > 0 )
		{
			dragReductionScalar = 1.5f;
		}
		else
		{
			dragReductionScalar = 1.0f;
		}

		// Rotation mechanism
		// TODO: We don't need rotation. That's what Rift is for.
		//		float controlsRotation = Input.GetAxis("Horizontal") * rotationSpeed;
		//		controlsRotation *= Time.deltaTime;
		//		transform.Rotate(0, controlsRotation, 0);
		if( Input.GetKey(KeyCode.P) )
		{
			transform.position += (fwdViewTransform.forward * panningSpeed) * Time.deltaTime;
		}

		// Run the base class update.
		base.Update();
	}

	void OnTriggerEnter(Collider other) 
	{
		if( other.tag == "Parachute" )
		{
			// Parachute acquired. Beginning equip phase.
	        Debug.Log("Player " + uLink.Network.player.id + " caught parachute!");

	        // Notify the server that you have the parachute, so that the server removes
	        // it from the game and assigns it to the player.
			uLink.NetworkView.Get(other.gameObject).RPC("ParachuteAcquiredCallback", uLink.NetworkPlayer.server, uLink.Network.player.id );
		
			// Disable the game object's collider in order to avoid repeat collision
			// before the RPC is sent and received.
			other.enabled = false;
		}
	}
}
