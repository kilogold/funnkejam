﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class FallingObject : uLink.MonoBehaviour 
{
	public float localGravity;
    
	public float dragReductionScalar = 1;
	
	public float fallSpeed;

	// Use this for initialization
	public virtual void Start () 
	{
		localGravity= GameplayProperties.PhysicsGravity;
	}
	
	// Update is called once per frame
	public virtual void Update () 
	{
		fallSpeed = localGravity * dragReductionScalar;
		transform.position += new Vector3( 0, -(fallSpeed * Time.deltaTime), 0 );
	}
}
