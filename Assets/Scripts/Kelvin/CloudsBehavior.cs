﻿using UnityEngine;
using System.Collections;

public class CloudsBehavior : MonoBehaviour 
{
	public float movementSpeed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position += new Vector3(0, movementSpeed * Time.deltaTime,0);
	}
}
