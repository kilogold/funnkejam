﻿using UnityEngine;
using System.Collections;

public class Animations : MonoBehaviour {

 
	public GameObject cubeDude;


	// Use this for initialization
	void Start () {

		//Debug.Log (cubeDude.animation ["Take 001"].length);

		cubeDude.animation.AddClip(cubeDude.animation.clip, "Falling", 1, 30, false);
		cubeDude.animation["Falling"].wrapMode = WrapMode.Loop;
		cubeDude.animation.AddClip(cubeDude.animation.clip, "LookWatch", 35, 69, true);
		cubeDude.animation.AddClip(cubeDude.animation.clip, "Hit", 69, 98, true);
		cubeDude.animation.AddClip(cubeDude.animation.clip, "FIRE", 103, 132, true);
		cubeDude.animation.AddClip(cubeDude.animation.clip, "OpenParachute", 200, 230, false);
		cubeDude.animation.AddClip(cubeDude.animation.clip, "GrabParachute", 137, 195, false);
		//cubeDude.animation["GrabParachute"].wrapMode = WrapMode.Loop;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI() {
		
		if (GUI.Button (new Rect (10, 50, 100, 30), "Falling"))
			PlayAnimationState ("Falling");
			//cubeDude.animation["Take 001"].time = 0.0f;
			
		if (GUI.Button (new Rect (10, 90, 100, 30), "LookWatch"))
			PlayAnimationState ("LookWatch");
			//cubeDude.animation["Take 001"].time = 0.5f;
			
		if (GUI.Button (new Rect (10, 130, 100, 30), "Hit"))
			PlayAnimationState ("Hit");
			//cubeDude.animation["Take 001"].time = 1.0f;
			
		if (GUI.Button (new Rect (10, 170, 100, 30), "FIRE"))
			PlayAnimationState ("FIRE");
			//cubeDude.animation["Take 001"].time = 1.5f;
			
		if (GUI.Button (new Rect (10, 210, 105, 30), "Open Parachute"))
			PlayAnimationState ("OpenParachute");
			//cubeDude.animation["Take 001"].time = 2.0f;
			
		if (GUI.Button (new Rect (10, 250, 105, 30), "Grab Parachute"))
			PlayAnimationState ("GrabParachute");
			//cubeDude.animation["Take 001"].time = 3.5f;
			
		//Debug.Log("Clicked the button with text");
						
	}

	void PlayAnimationState(string animationState){

		//Debug.Log ( animationState );
		cubeDude.animation.Play(animationState);

	}

}
