﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FunnkePlugin : MonoBehaviour
{
	AndroidJavaClass funnkeSDK;
	public string MRN_APP_KEY = "";
	public string MRN_SECRET = "";
	public bool DEBUGGING = false;
	public bool useVirtualNumber = false;
	public bool dontDestoryOnLoad = true;


	static FunnkePlugin myInstance;

	public static FunnkePlugin Instance { get { return myInstance; } }

	// Use this for initialization
	void Awake() {
		if (this.DEBUGGING) {
			Debug.Log("Awake");
			Debug.Log(Application.platform);
		}

		myInstance = this;
		if (this.dontDestoryOnLoad)
			DontDestroyOnLoad(myInstance);

		if (Application.platform == RuntimePlatform.Android) {
			AndroidJNI.AttachCurrentThread();
			funnkeSDK = new AndroidJavaClass("com.hookmobile.mrn.MrnManager");
			using (AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) { 
				AndroidJavaObject activty = jc.GetStatic<AndroidJavaObject>("currentActivity");

				object[] initParams = new object[5];
				initParams [0] = activty;
				initParams [1] = MRN_APP_KEY;
				initParams [2] = MRN_SECRET;
				initParams [3] = DEBUGGING;
				initParams [4] = useVirtualNumber;
				
				funnkeSDK.CallStatic("activate", initParams);
			}
		}
		
	}

	void Start() {
		if (this.DEBUGGING)
			Debug.Log("Start");
		if(funnkeSDK != null)
			funnkeSDK.CallStatic<AndroidJavaObject>("getInstance").Call("createSession");
	}
		
	void OnDestroy() {
		if (this.DEBUGGING)
			Debug.Log("OnDestroy");
		if(funnkeSDK != null)
			funnkeSDK.CallStatic<AndroidJavaObject>("getInstance").Call("release");
	}

	public void Init() {
		this.Awake();
	}

	public bool OpenOfferWall() {
		if (this.DEBUGGING)
			Debug.Log("OpenOfferWall");

		bool result = false;
		if (funnkeSDK != null)
			result = funnkeSDK.CallStatic<AndroidJavaObject>("getInstance").Call<bool>("openOfferWall");

		Debug.Log ( funnkeSDK );

		return result;
	}

	public Dictionary<int, int> GetPendingRewards() {
		if (this.DEBUGGING)
			Debug.Log("GetPendingRewards");

		Dictionary<int, int> results = new Dictionary<int, int> ();
		if (funnkeSDK != null)
			results = funnkeSDK.CallStatic<AndroidJavaObject>("getInstance").Call<Dictionary<int, int>>("getPendingRewards");

		return results;
	}

	public bool CompleteReward(int transactionId) {
		if (this.DEBUGGING)
			Debug.Log("CompleteReward");

		bool result = false;
		if (funnkeSDK != null)
			return funnkeSDK.CallStatic<AndroidJavaObject>("getInstance").Call<bool>("completeReward", transactionId);

		return result;
	}
}

